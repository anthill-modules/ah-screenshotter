const fs = require('fs');
const Path = require('path');
const Pageres = require('pageres');

const batch = require('batch-iterator');

var defaultOptions = {
    delay: 1,
    crop:true,
    filename:"thumb-<%= size %>"
};

var defaultConfig = {
    batchSize: 10,
    dest: './',
    resolutions: ['1024x768'],
    port: '3000'
};

var api = {
    single: function(url, config, options, next) {
        options = options || defaultOptions;
        var dest = config.dest || './';
        var startTime = Date.now();

        var pageres = new Pageres(options);
        return pageres
            .src(url, config.resolutions)
            .dest(dest)
            .run()
            .then(function() {
                var endTime, diffTime;
                endTime = Date.now();
                diffTime = (endTime - startTime) / 1000;
                console.log("Screenshot captured in " + diffTime + " seconds.");
                next(null, url, dest);
            })
            .catch(function(err) {
                console.log(err);
                next(err);
            });
    },
    // Take screenshots of local files and remote urls
    urls: function(urls, config, options, next) {
        options = options || defaultOptions;
        var batchSize = config.batchSize || defaultConfig.batchSize;
        var dest = config.dest || defaultConfig.dest;
        var resolutions = config.resolutions || defaultConfig.resolutions;
        var startTime = Date.now();
        var screenshots = [];

        function getDest(url) {
            var data = Path.parse(url);
            if (data.dir === 'http:/' || data.dir === 'https:/') {
                return Path.resolve(dest, data.name);
            }
            else {
                return Path.resolve(data.dir, dest);
            }
        }

        // Loop through each URL/path
        batch(urls, batchSize, function(url) {
            var screenshotDestination = getDest(url);
            var pageres = new Pageres(options);
            screenshots.push(screenshotDestination);
            return pageres
                .src(url, resolutions)
                .dest(screenshotDestination)
                .run();
        })
            .then(function() {
                var endTime, diffTime;
                endTime = Date.now();
                diffTime = (endTime - startTime) / 1000;
                console.log("Screenshots captured in " + diffTime + " seconds.");
                next(null, urls, screenshots);
            })
            .catch(function(err) {
                console.log(err);
                next(err);
            });
    },
    // Take screenshots for Veeva wide slides
    // where Veeva id !== slide id
    // Requires that the presentation is running on localhost
    veeva: function(urls, config, options, next) {
        options = options || defaultOptions;
        var batchSize = config.batchSize || defaultConfig.batchSize;
        var dest = config.dest || defaultConfig.dest;
        var resolutions = config.resolutions || defaultConfig.resolutions;
        var PORT = config.port || defaultConfig.port;
        var startTime = Date.now();
        var screenshots = [];

        // Loop through each URL/path
        // Each url will be in format of: veevaId/slideId
        batch(urls, batchSize, function(url) {
            var info = url.split('/');
            var screenshotDestination = dest + '/' + info[0]; //getDest(url);
            var pageres = new Pageres(options);
            screenshots.push(screenshotDestination);
            return pageres
                .src('http://localhost:' + PORT + '/?path=' + info[1], resolutions)
                .dest(screenshotDestination)
                .run();
        })
            .then(function() {
                var endTime, diffTime;
                endTime = Date.now();
                diffTime = (endTime - startTime) / 1000;
                console.log("Screenshots captured in " + diffTime + " seconds.");
                next(null, urls, screenshots);
            })
            .catch(function(err) {
                console.log(err);
                next(err);
            });
    }
};

module.exports = api;
