# Anthill Screenshotter

Capture screenshots of URLs or local web pages.

Screenshots are captured in batches for optimal performance.

## Installation

```
npm install ah-screenshotter --save
```

## Usage

```
var screenshotter = require('ah-screenshotter');

screenshotter(urls, config, options, next);
```

## URLs

URLs is an array of URLs or local paths that should be screenshotted.

Example:
```
["https://github.com", "slides/home/index.html"]
```

## Config

- resolutions ARRAY - e.g. ['1024x768', '768x1024']
- dest STRING - Path to where the images will be created
- batchSize Int - Number of URL per batch. [10]

Note that **dest** should be relative from the default location.

E.g. this is default:

- "https://github.com" -> ./github.com/[filename].png
- "slides/home/index.html" -> ./slides/home/[filename].png

A **dest** of "thumbs" would result in:

- ./github.com/thumbs/[filename].png
- ./slides/home/thumbs/[filename].png

## Options

The module is a wrapper around [Pageres](https://github.com/sindresorhus/pageres#pageresoptions)
and the settings will be passed to the Pageres constructor.

See the official docs for all settings. Here are a few important ones:

- delay INT - Delay in seconds before capture
- crop BOOL - If image should be cropped to resolution
- filename STRING - What to name the images