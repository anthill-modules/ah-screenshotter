"use strict";

const test = require('tap').test;
const Path = require('path');
const fs = require('fs');

const screenshotter = require('../index');

const simplePath = Path.join(__dirname, 'content/simple/');
const complexPath = Path.join(__dirname, 'content/faq-module/');

var config = {
    dest: 'test/screenshots'
};

test("Screenshots of web urls", function(t) {
    screenshotter.urls(['http://www.anthillagency.com', 'http://google.com'], config, {}, function(err, files, dest) {
        console.log(files, dest);
        // fs.readFile(copyPath + 'index.html', 'utf8', function(error, content) {
        t.error(err);
     // t.ok(/id="copy_of_simple"/.test(content));
        t.ok(files);
        t.ok(dest);
        t.end();
        // });
    });
});

test("Screenshots of Veeva slides", function(t) {
    screenshotter.veeva(['acs_slideshow_AcuteACSSlide/AcuteACSSlide', 'full_story_pe_slideshow_ITTAnalysisSlide/ITTAnalysisSlide'], config, {delay: 2, crop: false, filename: 'thumb'}, function(err, files, dest) {
        console.log(files, dest);
        // fs.readFile(copyPath + 'index.html', 'utf8', function(error, content) {
        t.error(err);
        // t.ok(/id="copy_of_simple"/.test(content));
        t.ok(files);
        t.ok(dest);
        t.end();
        // });
    });
});
